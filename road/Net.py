import requests
from lxml import etree
from road.File import replace_clear_array, filter_match


def convert_to_tree_from_url(url):
    useragent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3319.267 Safari/537.36'
    }
    html = requests.get(url, headers=useragent)
    html.encoding = 'utf-8'
    treeNode = etree.HTML(html.content.decode('utf-8'))
    return treeNode


def get_html_from_url(url):
    useragent = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3319.267 Safari/537.36'
    }
    html = requests.get(url, headers=useragent)
    html.encoding = 'utf-8'
    return html.content.decode('utf-8')


def tree_xpath(treeNode, treepath, attr, **atype):
    try:
        content = treeNode.xpath(treepath)
    except:
        content = []
    if len(content) <= 0:
        if atype == 'integer':
            return -1
        return ''
    else:
        # print(content)
        # print(content[0])
        if attr == '':
            return content[0].replace(' ', '')  # strip()
        elif attr == 'space':
            return content[0].text
        elif attr == 'text':
            return content[0].text.replace(' ', '')  # .strip()
        elif attr == 'string':
            return content[0].xpath('string(.)').replace(' ', '')  # .strip()
        elif attr == 'count':  # 标签个数
            return content


def analysis_define_field(treeNode, define_field, index, level):
    entry = []
    i = index
    for item in define_field:
        if item['level'] == level:
            # entry = []
            # had value
            if item['category'] == 'text':
                # entry[item['name']] = item['value']
                entry.append({
                    'name': item['name'],
                    'value': item['value']
                })
            if item['category'] == 'xpath':
                treepath = item['treepath'].replace('#index', str(i+1))
                if len(item['expatype']) <= 0:
                    value = tree_xpath(treeNode, treepath, item['expattr'])
                else:
                    value = tree_xpath(treeNode, treepath,
                                       item['expattr'], item['expatype'])
                # need replace data
                if len(item['cleararray']) > 0:
                    value = replace_clear_array(value, item['cleararray'])
                # need regex for twice match
                if len(item['regex']) > 0:
                    value = filter_match(value, item['regex'])
                entry.append(
                    {
                        'name': item['name'],
                        'value': value
                    }
                )
            # entry[item['name']] = value
    # print(entry)
    return entry


def analysis_define_config(treeNode, define_config, index, level):
    entry = []
    i = index
    for item in define_config:
        if item['node']['level'] == level:
            # had value
            if item['node']['category'] == 'text':
                entry.append({
                    'name': item['name'],
                    'value': item['value']
                })
            if item['node']['category'] == 'xpath':
                treepath = item['node']['treepath'].replace(
                    '(#index)', str(i+1))
                if len(item['node']['expatype']) <= 0:
                    value = tree_xpath(treeNode, treepath,
                                       item['node']['expattr'])
                else:
                    value = tree_xpath(treeNode, treepath,
                                       item['node']['expattr'], item['node']['expatype'])
                # need replace data
                if len(item['node']['cleararray']) > 0:
                    value = replace_clear_array(
                        value, item['node']['cleararray'])
                # need regex for twice match
                if len(item['node']['regex']) > 0:
                    value = filter_match(value, item['node']['regex'])
                entry.append(
                    {
                        'name': item['name'],
                        'value': value
                    }
                )
    return entry
