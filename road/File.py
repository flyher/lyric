import os
import re
import json


def save_to_file(content, path, file, is_append):
    if os.path.exists(path) == False:
        os.mkdir(path)
    try:
        if is_append:
            file = open(path+'/'+file, 'a')  # append
            file.write(content+'\n')
        else:
            file = open(path+'/'+file, 'w+')  # overwrite
            file.write(content)
        file.close()
        return True
    except Exception as e:
        print(e)
        return False


def read_from_file(path, encode):
    file = open(path, 'r', encoding=encode)
    content = file.read()
    # content=file.readline()
    file.close()
    return content


def replace_clear_array(content, clear_array):
    str = content
    for item in clear_array:
        str = str.replace(item, '')
    return str

# def replace_array(content,array):


def filter_match(content, regex):
    str = re.search(regex, content)
    return str == None and '' or str.group()


def convert_str_to_json(content):
    return json.loads(content)
