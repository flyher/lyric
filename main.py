import sqlite3
from lxml import etree
import datetime
import requests
import decimal
from dateutil.relativedelta import relativedelta  # 月份

from lyric.music163 import get_artist_and_song_name


def test():
    url = 'https://music.163.com/#/song?id=189688'
    get_artist_and_song_name(url)


if __name__ == "__main__":
    test()
