from road.Net import convert_to_tree_from_url, analysis_define_config, get_html_from_url
from road.File import convert_str_to_json, save_to_file
from lyric.lyric import merge_lyric

define_config = {
    'lyric_url_api': 'https://music.163.com/api/song/media?id=(#id)',
    'song_url_api': 'https://music.163.com/#/song?id=(#id)',
    'song_url_m_api': 'https://music.163.com/m/song?id=(#id)',
    'song_url_host': 'https://music.163.com/#/song?id=',
    'song_url_m_host': 'https://music.163.com/m/song?id=',
    'xpath': [
        {
            'name': 'Title',
            'desribe': '标题',
            'node': {
                'category': 'xpath',
                'level': 1,
                'treepath': '/html/head/title',
                'expattr': 'string',
                'expatype': '',
                'cleararray': [],
                'regex':''
            },
            'sql': {
                'sqltype': 'text',
                'needinsert': True,
            }
        },
        {
            'name': 'Album',
            'desribe': '专辑',
            'node': {
                'category': 'xpath',
                'level': 1,
                'treepath': '/html/head/meta[@name="description"]/@content',
                'expattr': '',
                'expatype': '',
                'cleararray': [],
                'regex':''
            },
            'sql': {
                'sqltype': 'text',
                'needinsert': True,
            }
        }
    ]
}


def get_lyric_url_from_id(id: 'song id'):
    # id获取lyric地址
    return define_config['lyric_url_api'].replace('(#id)', str(id))


def get_id_from_song_url(url: 'song url'):
    # 从url中提取id
    return url.replace(define_config['song_url_host'], '').replace('song_url_m_host', '')


def get_m_url_from_id(id):
    return define_config['song_url_host']


def convert_song_url_to_song_url_m(url: 'song url for desktop'):
    return url.replace('/#/', '/m/')


def get_artist_and_song_name(url: 'song url'):
    id = get_id_from_song_url(url)
    treeNode = convert_to_tree_from_url(convert_song_url_to_song_url_m(url))
    entry = analysis_define_config(treeNode, define_config['xpath'], -1, 1)
    # print(entry)
    title = entry[0]['value'].split('-')
    albuminfo = entry[1]['value'].split('。')
    artist = ''
    songName = ''
    album = ''
    if len(title) >= 2:
        artist = title[1]
        songName = title[0]
        # print(artist, songName)
    if len(albuminfo) >= 2:
        album = albuminfo[1].replace('所属专辑：', '')
    lyric_url = get_lyric_url_from_id(id)
    lyric_content = get_html_from_url(lyric_url)
    lyric_json = convert_str_to_json(lyric_content)

    lyric_all = merge_lyric(album, artist, '未知', '网易云音乐',
                            songName, lyric_json['lyric'])

    lyric_name = artist+' - '+songName+'.lrc'
    save_to_file(lyric_all, './down/', lyric_name, False)
