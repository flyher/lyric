def create_header(album, artist, author, source, songName):
    header = "[al:(#album)]\n[ar:演出者-(#artist)]\n[au:歌詞作者-(#author)]\n[by:source from (#source)]\n[re:lyric v0.1]\n[ti:(#songName)]\n[ve:lyric v0.1]\n"
    return header.replace('(#album)', album).replace('(#artist)', artist).replace('(#author)', author).replace('(#source)', source).replace('(#songName)', songName)


def merge_lyric(album, artist, author, source, songName, lyric):
    return create_header(album, artist, author, source, songName)+lyric
